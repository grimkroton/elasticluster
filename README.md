# What?
This is a Vagrant-Powered PoC for a ElasticSearch Cluster
# Usage
Just do a <code>vagrant up</code> and we are spinnning up 4 Vagrant-VMs:
* 3 ElasticSearch VMs as a cluster
* 1 Bastion VM as a management Server

After `vagrant up` has finished, you can do a `curl http://192.168.100.10:9200/_cluster/health?pretty` to see the cluster nodes.