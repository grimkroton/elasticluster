cat /etc/hosts
yum -y install epel-release
yum -y install python3-pip
pip3 install --upgrade pip
pip3 install ansible jmespath
/usr/local/bin/ansible-galaxy install elastic.elasticsearch,v7.11.1
chmod 400 /vagrant/ansible/ssh/id_rsa
cd /vagrant/ansible
/usr/local/bin/ansible-playbook -i /vagrant/ansible/inventory.ini /vagrant/ansible/elastic.yml
curl http://es1.muenchen.local:9200/_cluster/health?pretty